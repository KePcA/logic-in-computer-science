#!/usr/bin/python
# -*- coding: utf-8 -*-

import prop
import math
import bool_formulas as bf
import re



def sudoku(s):
    """Vrne logični izraz, ki opisuje sudoku s z abecedo abc."""
    """
    n = len(abc)
    r = int(math.sqrt(n))

    if isinstance(abc, basestring):
        s = [[None if s[i][j] in [None, ""] else str(s[i][j]) for j in range(n)] for i in range(n)]
        assert all([all([x == None or (len(x) == 1 and x in abc) for x in l]) for l in s]), "Sudoku vsebuje neveljavne simbole!"
    else:
        assert None not in abc, "Abeceda ne sme vsebovati None!"
        assert all([all([x == None or x in abc for x in l]) for l in s]), "Sudoku vsebuje neveljavne simbole!"

    assert n == r*r, "Velikost abecede ni popoln kvadrat!"
    assert len(s) == n, "Število vrstic se ne ujema s številom znakov!"
    assert all([len(l) == n for l in s]), "Število stolpcev se ne ujema s številom znakov!"
    """

    l = []

    for i in range(9):
        for j in range(9):
            if s[i][j] != None:
                # Obstoječa števila
                t = s[i][j]
                for k in range(9):
                    if k == t:
                        l.append(bf.Var("r%dc%dv%d" % (i, j, k)))
                    else:
                        l.append(bf.Not(bf.Var("r%dc%dv%d" % (i, j, k))))
            else:
                # Vsako polje ima natanko eno vrednost
                for k in range(9):
                    l.append(bf.Or([bf.Not(bf.Var("r%dc%do%d" % (i, j, k))), bf.And([bf.Var("r%dc%dv%d" % (i, j, x)) if x == k else bf.Not(bf.Var("r%dc%dv%d" % (i, j, x))) for x in range(9)])]))
                l.append(bf.Or([bf.Var("r%dc%do%d" % (i, j, k)) for k in range(9)]))
            # V vsaki vrstici se pojavi vsaka vrednost
            l.append(bf.Or([bf.Var("r%dc%dv%d" % (j, x, i)) for x in range(9)]))
            # V vsakem stolpcu se pojavi vsaka vrednost
            l.append(bf.Or([bf.Var("r%dc%dv%d" % (x, j, i)) for x in range(9)]))

        # V vsakem kvadratu se pojavi vsaka vrednost
        for j in range(3):
            for k in range(3):
                l.append(bf.Or(sum([["r%dc%dv%d" % (3*j+x, 3*k+y, i) for x in range(3)] for y in range(3)], [])))

    return bf.And(l)

sudoku1 = [[None, 8, None, 1, 6, None, None, None, 7],
		[1, None, 7, 4, None, 3, 6, None, None],
		[3, None, None, 5, None, None, 4, 2, None],
		[None, 9, None, None, 3, 2, 7, None, 4],
		[None, None, None, None, None, None, None, None, None],
		[2, None, 4, 8, 1, None, None, 6, None],
		[None, 4, 1, None, None, 8, None, None, 6],
		[None, None, 6, 7, None, 1, 9, None, 3],
		[7, None, None, None, 9, 6, None, 4, None]]



for j in range(3):
    for k in range(3):
        print sum([["r%dc%dv%d" % (3*j+x, 3*k+y, 1) for x in range(3)] for y in range(3)], [])

print "\n"


for j in range(3):
    for k in range(3):
        for y in range(3):
            for x in range(3):
                print "r%dc%dv%d" % (3*j+x, 3*k+y, 1)